# README.md

MFDOG: the Masks For Docs Onboarding Guide. A kinder, gentler guide to help you get started with the Masks For Docs Logistics Supplies Platform effort.

## Sections

- [Introduction](#Introduction)
- [Order Of Operations](#order-of-operations)
- [Our Ask](#our-ask)

## Introduction

Welcome to Masks For Docs! If you need help getting started with contributing to the Logistics Supplies Platform, this is the place to start. This guide should serve to get people onboard until we can automate some of the account creation and developer environment setup.

> **TO-DO** Update the intro

## Order Of Operations

1. First, start with [Common](./Common.md) to become familiar with the architecture and documentation, see the stack components, and the location of the source code.

1. After all of the Common items are completed and installed, proceed to the guide that applies to your specific role. Please note that FE and BE developers could be asked to help with one or the other, hence a single onboarding guide.

    <!-- - [508 Tester](./508.md) -->
    - [Developer (FE & BE)](./Developer.md)
    - [DevOps Engineer](./DevOps.md)
    <!-- - [Functional Tester](./FunctionalTester.md) -->
    <!-- - [Performance Tester](./PerfTester.md)  -->
    <!-- - [Platform Tester](./PlatTester.md) -->
    <!-- - [Test Automator](./TestAutomator.md) -->
    <!-- - [Test Data Manager](./TDM.md) -->

    Each page includes the names of people who can help you. Don't see your role listed? Check the source of this repo to see if we don't already have a page in waiting, and/or create a new one with the [Role Template](./RoleTemplate.md), or just [let us know!][mfdog_questions_email]

> **TO-DO** Are there any other high-level steps at a common level that have to be done after the volunteer moves on to her/his role-specific guide?

## Our Ask

If you see something in this documentation that needs fixing or correcting, please take a moment to submit a pull request with your correction. Whether it's a one-letter typo or a larger update to how we do things, it's very important that we all work together to make this process faster, easier and better for future team members. If you have questions, please ask within GitLab or [let us know!][pri_maintainer_email]

[pri_maintainer_email]: mailto:nuwayser@gmail.com?subject=mfdog-q-README

# DevOps - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for new CI/CD team members. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to help automate the development, testing and deployment processes. Bring work to the people (not people to the work)!

> **TO-DO** Update the intro

## Teammates Who Can Help

1. The maintainer of this document is [Pete Nuwayser][pri_maintainer_email] (DC GMT-5). Pete can help get you started and pointed in the right direction.

1. The DevOps lead is [Ariana Bray][devops_lead_email] (Seattle GMT-8).

1. Team members: Pete Nuwayser, Cory Parent

> **TO-DO** Add other DevOps names and put list in a more logical order if desired

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role

> **TO-DO** add prerequisites for FE and BE that didn't belong in Common.

## Required Accounts

> **TO-DO** this should be for any accounts requiring manual setup, or other things that may need to be approved, that are not included in Common. The hope is to automate account creation as much as possible.

## Required Software

1. This

1. That

1. The other thing

1. Specific to Development and NOT in Common already

## Other Resources Needed

1. Set Environment Variables

1. Whatever else

[pete_email]: mailto:nuwayser@gmail.com?subject=mfdog-q-DevOps

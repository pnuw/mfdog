# Developer - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for front-end and back-end developers. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to float between FE and BE work as needed. Bring work to the people (not people to the work)!

- The backend repository is located [here][be_repo_url].
- The frontend repository is location [here][fe_repo_url].

> **TO-DO** Update the intro

## Teammates Who Can Help

1. The maintainer of this document is [Pete Nuwayser][pri_maintainer_email] (DC GMT-5). Pete can help get you started and pointed in the right direction.

1. The Front End lead is [David Golightly][fe_lead_email].

1. The Back End lead is [Taylor Fairbank][be_lead_email] (Prague GMT+1).

1. Other team members: Bion Johnson, etc.

> **TO-DO** add more people!

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role

> **TO-DO** add prerequisites for FE and BE that didn't belong in Common.

## Required Accounts

> **TO-DO** this should be for any accounts requiring manual setup, or other things that may need to be approved, that are not included in Common. The hope is to automate account creation as much as possible.

## Required Software

1. This

1. That

1. The other thing

1. Specific to Development and NOT in Common already

## Other Resources Needed

1. Set Environment Variables

1. Whatever else

[pri_maintainer_email]: mailto:nuwayser@gmail.com?subject=mfdog-q-Developer

<!-- [be_repo_url]: https://gitlab.com/distribute-aid/toolbox -->
[be_repo_url]: https://gitlab.com/masksfordocs/toolbox
[fe_repo_url]: https://gitlab.com/humanity-united/toolbox

# <role_name> - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

> **TO-DO** Write an intro: what role is this for? How is it different from the existing roles? What is it used for?

## Teammates Who Can Help

1. list names here [name][name_email]

> **TO-DO** Decide what information to include here: name, email, phone number, maybe a designation like Primary vs Backup
> **TO-DO** update the email tags at the bottom of this document

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role

1. list other prerequisites here

## Required Accounts

1. list all accounts that are unique to this role in this section

## Required Software

1. list all software that is unique to this role in this section

> **TO-DO** Create an actionable format with links to existing resources
> **TO-DO** Indicate the proper test to confirm that the software is installed correctly

## Other Resources Needed

1. Set Environment Variables

[name_email]: mailto:name_email?subject=mfdog-q-<role_name>


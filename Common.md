# Common Environment - Start Here  

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)
- [Other Resources Needed](#other-resources-needed)

## Introduction

- The Platform Roadmap is located [here][roadmap_url]. It lays out an approach to moving Masks For Docs (MDF) from its current Typeforms + Google Sheets process to one that is more robust, scalable, and efficient.

- Placeholder for Josh Rickard's architecture document

> **TO-DO** Update the intro with links to the architecture, integrations, whatever other high-level info people need to get started.

## Teammates Who Can Help

1. The maintainer of this document is [Pete Nuwayser][pri_maintainer_email] (DC GMT-5). Pete can help get you started and pointed in the right direction.

1. The Front End lead is [David Golightly][fe_lead_email].

1. The Back End lead is [Taylor Fairbank][be_lead_email] (Prague GMT+1).

1. The DevOps lead is [Ariana Bray][devops_lead_email] (Seattle GMT-8).

> **TO-DO** Possible to have POC contacts go straight to slack and not use email?
> **TO-DO** Add some more names for resiliency

## Prerequisites

1. Get on the [slack][slack_join_url] and join #logistics-supplies-platform as well as your local group e.g. #zlocal-us-ny-nyc

> **TO-DO** gather top-level prerequisites from FE, BE and DevOps leads - may need a separate table for this, then gather stuff from the leads to figure out which things are common

## Required Accounts

> **TO-DO** this should be for any accounts requiring manual setup, or other things that may need to be approved. The hope is to automate account creation as much as possible.

## Required Software

1. Google Chrome

> **TO-DO** gather all software requirements from FE, BE and DevOps leads via a table/wiki, then boil down to common stuff to include here vs in the other sub-guides

## Other Resources Needed

1. Nothing yet

[pri_maintainer_email]: mailto:nuwayser@gmail.com?subject=mfdog-q-Common
[fe_lead_email]:
[be_lead_email]:
[devops_lead_email]:

[roadmap_url]: https://docs.google.com/document/d/1EH7ygTBbkb7_HzPqBetiEDXaxEAkzbpTh997ICGZw5M/mobilebasic#
[slack_join_url]: https://slack-invite.masksfordocs.com
